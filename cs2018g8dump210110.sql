-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 03:31 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs2018g8`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `Dri_ID` int(2) NOT NULL,
  `Dri_Name` varchar(50) NOT NULL,
  `Dri_NIC` varchar(10) NOT NULL,
  `Dri_Licence` varchar(10) NOT NULL,
  `Dri_Contact_No` int(10) NOT NULL,
  `Belongs_To` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`Dri_ID`, `Dri_Name`, `Dri_NIC`, `Dri_Licence`, `Dri_Contact_No`, `Belongs_To`) VALUES
(1, 'N. K. Samarasinghe', '826981366V', 'B2459875', 769853254, 'Faculty of Science'),
(2, 'S. P. Amarasinghe', '803696944V', 'B4789652', 719856345, 'Administration'),
(3, 'A. K. N. Silva', '852368480V', 'B6982356', 778956422, 'Faculty of Science');

-- --------------------------------------------------------

--
-- Table structure for table `journey`
--

CREATE TABLE `journey` (
  `Jour_ID` int(10) NOT NULL,
  `Jour_Start_Meter` float NOT NULL,
  `Jour_Start_Time` time NOT NULL,
  `Jour_Start_Date` date NOT NULL,
  `Jour_End_Meter` float DEFAULT NULL,
  `Jour_End_Time` time DEFAULT NULL,
  `Jour_End_Date` date DEFAULT NULL,
  `Req_ID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `Req_ID` int(10) NOT NULL,
  `User_ID` varchar(20) NOT NULL,
  `Reason` varchar(100) NOT NULL,
  `Reason_Description` varchar(500) DEFAULT NULL,
  `Organized_By` varchar(500) NOT NULL,
  `Required_Date` date NOT NULL,
  `Required_Time` time NOT NULL,
  `Dur_Days` int(11) DEFAULT NULL,
  `Dur_Hours` int(11) NOT NULL,
  `Distance` int(11) NOT NULL,
  `No_Of_Passengers` int(11) NOT NULL,
  `Names_Of_Staff_Participants` varchar(50000) DEFAULT NULL,
  `Names_Of_Students` blob,
  `Invitation` blob NOT NULL,
  `Destination` varchar(100) NOT NULL,
  `Departure_Location` varchar(100) NOT NULL,
  `Assigned_Vehicle` varchar(50) DEFAULT NULL,
  `Assigned_Driver` varchar(50) DEFAULT NULL,
  `Reason_For_Rejection` varchar(250) DEFAULT NULL,
  `forward_position` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`Req_ID`, `User_ID`, `Reason`, `Reason_Description`, `Organized_By`, `Required_Date`, `Required_Time`, `Dur_Days`, `Dur_Hours`, `Distance`, `No_Of_Passengers`, `Names_Of_Staff_Participants`, `Names_Of_Students`, `Invitation`, `Destination`, `Departure_Location`, `Assigned_Vehicle`, `Assigned_Driver`, `Reason_For_Rejection`, `forward_position`) VALUES
(13, 'UOR/FS/8632', 'To Collect Specimen', NULL, 'NSF', '2019-01-31', '00:00:00', 1, 4, 50, 2, 'UOR/FS/2300\r\nUOR/FS/1456', '', '', 'Galle', 'University of Ruhuna', NULL, NULL, NULL, 'VC'),
(14, 'UOR/FS/2364', 'For Reaseach Purposes', NULL, 'UGC', '2019-02-13', '08:00:00', 0, 9, 100, 1, 'UOR/FS/2300', '', '', 'Colombo', 'University of Ruhuna', NULL, NULL, NULL, 'DEAN APPROVED'),
(15, 'UOR/FS/7524', 'For Reaseach Purposes', NULL, 'UGC', '2019-02-13', '08:00:00', 0, 9, 100, 1, 'UOR/FS/2300', '', '', 'Colombo', 'University of Ruhuna', NULL, NULL, NULL, 'VC APPROVED'),
(16, 'UOR/FS/7524', 'For Reaseach Purposes', NULL, 'UGC', '2019-02-13', '08:00:00', 0, 9, 100, 1, 'UOR/FS/2300', '', '', 'Colombo', 'University of Ruhuna', NULL, NULL, NULL, 'DEAN'),
(17, 'UOR/FS/8632', 'For Reaseach Purposes', NULL, 'UGC', '2019-02-13', '08:00:00', 0, 9, 100, 1, 'UOR/FS/2300', '', '', 'Colombo', 'University of Ruhuna', NULL, NULL, NULL, 'DEAN REJECT'),
(18, 'UOR/FS/1632', 'For Reaseach Purposes', NULL, 'UGC', '2019-02-13', '08:00:00', 0, 9, 100, 1, 'UOR/FS/2300', NULL, '', 'Colombo', 'University of Ruhuna', NULL, NULL, NULL, 'DEAN APPROVED'),
(19, 'UOR/FS/1632', 'To Collect Specimen', NULL, 'NSF', '2019-01-31', '00:00:00', 1, 4, 50, 2, 'UOR/FS/2300\r\nUOR/FS/1456', NULL, '', 'Galle', 'University of Ruhuna', NULL, NULL, NULL, 'DEAN REJECT'),
(20, 'UOR/AD/011', 'To Collect Specimen', '', 'NSF', '2019-01-15', '08:00:00', 1, 9, 0, 3, 'ranasinghe\r\ngunawatdena', '', '', 'Colombo', 'University of Ruhuna', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `Status_ID` int(50) NOT NULL,
  `Req_Status` varchar(75) NOT NULL,
  `Req_ID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_ID` varchar(15) NOT NULL,
  `password` varchar(250) NOT NULL,
  `User_Name` varchar(100) NOT NULL,
  `Department` varchar(50) NOT NULL,
  `Role` varchar(20) NOT NULL,
  `User_Contact_No` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_ID`, `password`, `User_Name`, `Department`, `Role`, `User_Contact_No`) VALUES
('UOR/FS/1200', 'b9130c656a1cd6feb47de244145189c1', 'K. M. Fernando', 'Staff', 'Assistant Registrar', 776523013),
('UOR/FS/1500', 'aabf11b3599bbdf37f8f6020e467fd2d', 'K. T. Perera', 'Staff', 'Dean', 782356985),
('UOR/FS/2300', 'ad4ac7fa40b0af2bae7374c57173f26c', 'P. S. Kulathilaka', 'Staff', 'Clerk', 762389563),
('UOR/FS/6523', '4d19b37a2c399deace9082d464930022', 'P. Ekanayake', 'Chemistry', 'Department Head', 756325698),
('UOR/FS/7524', '58ec998e5f04921d22afdd67759db6e4', 'A. R. Soyza', 'Chemistry', 'Lecturer', 78965231),
('UOR/FS/8632', '5c8cb735a1ce65dac514233cbd5576d6', 'F. D. Karunarathne', 'Chemistry', 'Lecturer', 714562352);

-- --------------------------------------------------------

--
-- Table structure for table `userrequest`
--

CREATE TABLE `userrequest` (
  `User_ID` varchar(15) NOT NULL,
  `Req_ID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `Veh_ID` int(2) NOT NULL,
  `Dri_ID` int(2) NOT NULL,
  `Veh_No` varchar(10) NOT NULL,
  `Veh_Type` varchar(15) NOT NULL,
  `No_Of_Seats` int(2) NOT NULL,
  `Veh_Availability` varchar(10) DEFAULT NULL,
  `Belongs_To` varchar(20) NOT NULL,
  `User_ID` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`Veh_ID`, `Dri_ID`, `Veh_No`, `Veh_Type`, `No_Of_Seats`, `Veh_Availability`, `Belongs_To`, `User_ID`) VALUES
(1, 1, 'GJ - 3600', 'Car', 3, '', 'Faculty of Science', 'UOR/FS/0020'),
(2, 3, 'SP - 3689', 'Cab', 10, NULL, 'Faculty of Science', 'UOR/FS/0020');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`Dri_ID`);

--
-- Indexes for table `journey`
--
ALTER TABLE `journey`
  ADD PRIMARY KEY (`Jour_ID`),
  ADD KEY `Req_ID` (`Req_ID`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`Req_ID`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`Status_ID`),
  ADD KEY `Req_ID` (`Req_ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_ID`);

--
-- Indexes for table `userrequest`
--
ALTER TABLE `userrequest`
  ADD PRIMARY KEY (`User_ID`,`Req_ID`),
  ADD KEY `Req_ID` (`Req_ID`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`Veh_ID`),
  ADD KEY `Dri_ID` (`Dri_ID`),
  ADD KEY `User_ID` (`User_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `Dri_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `journey`
--
ALTER TABLE `journey`
  MODIFY `Jour_ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `Req_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `Status_ID` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `Veh_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `journey`
--
ALTER TABLE `journey`
  ADD CONSTRAINT `journey_ibfk_1` FOREIGN KEY (`Req_ID`) REFERENCES `request` (`Req_ID`);

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`Req_ID`) REFERENCES `request` (`Req_ID`);

--
-- Constraints for table `userrequest`
--
ALTER TABLE `userrequest`
  ADD CONSTRAINT `userrequest_ibfk_2` FOREIGN KEY (`Req_ID`) REFERENCES `request` (`Req_ID`),
  ADD CONSTRAINT `userrequest_ibfk_3` FOREIGN KEY (`User_ID`) REFERENCES `user` (`User_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
