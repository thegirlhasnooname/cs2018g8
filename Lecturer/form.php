<?php 
session_start();
if(!isset($_SESSION['USER_NAME'])){
  return header("location:../UserLogin/userlogin.php");
}
$name=$_SESSION['USER_NAME'];
$Department=$_SESSION['DEPARTMENT'];
$EmployeeNo=$_SESSION['EMPLOYEENO'];
$Date = date("Y/m/d");
include("connection.php");

  if(isset($_GET['status'])==1){
     echo "<script type='text/javascript'>alert('successfully added')</script>";
  }

?>



<!DOCTYPE html>
<html lang="en">


  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Lecturer_Request_Form</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <style>
      .tt-dropdown-menu {
        background-color: #FFFFFF;
        border: 1px solid rgba(0, 0, 0, 0.2);
        border-radius: 8px;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        margin-top: 12px;
        padding: 8px 0;
        width: 422px;
      }
      .tt-suggestion {
        font-size: 24px;
        line-height: 24px;
        padding: 3px 20px;
      }
      .tt-suggestion.tt-is-under-cursor {
        background-color: #0097CF;
        color: #FFFFFF;
      }
      .tt-suggestion p {
        margin: 0;
      }
    </style>
    

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="http://www.sci.ruh.ac.lk/">Faculty Of Science</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
       
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
	         <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">2+</span>
          </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="Notification.php"><h6>Your Request is Approved....</h6><br>2018/12/12  12.12PM</a>
            <a class="dropdown-item" href="RejNotification.php"><h6>Your Request is Rejected....</h6><br>2018/10/15  09.00Am</a>
            <div class="dropdown-divider"></div>
        </li>
        
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="profile.php">Profile</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../UserLogin/userlogin.php" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="form.php">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Request Form</span>
          </a>
       </li>

	 <li class="nav-item ">
          <a class="nav-link" href="OldRequest.php">
            <i class="fas fa-fw fa-clock"></i>
            <span>Old Requests</span>
          </a>
       </li>


      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a ><h3>Request Form</h3></a>
            </li>
           
          </ol>
          <form action="insert_req_form.php" method="POST"><br>
            <div style="float: left; margin-left:20px;margin-right: 130px;" >    


                <b>Details about Applicant</b><br><br>
                <!--.table-->
                <table>
                <tr>
                    <td>Name<br></br></td>
                    <td><input type="text" name="name" class="form-control" <?php echo 'value="' .$name.'"';?> readonly></td>
                </tr>

                <tr><td>Employee No<br></br></td>
                    <td><input type="text" name="emp_no" class="form-control" <?php echo 'value="' .$EmployeeNo.'"';?> readonly></td>
                </tr>

                <tr>
                    <td >Department<br></br></td>
                    <td><input type="text" name="Department" class="form-control" <?php echo 'value="' .$Department.'"';?> readonly>
                </tr>
                <tr>
                    <td>Date<br></br></td>
                    <td><input  name="date" class="form-control" <?php echo 'value="' .$Date.'"';?> readonly></td>
                </tr>
                </table>
                <!--/.table-->
                <br><br>
              </div>

                                <b>Details about Journey</b>
                <br><br>
                <!--table-->
                <table>
                    <tr>
                        <td>Reason in brief <br></br></td>
                        <td>

                          <select name="reason" class="form-control">
                            <option  selected disabled>Select Request Type</option>
                            <option>For a Meeting</option>
                            <option>To Present Pastpaper</option>
                            <option>For Reaseach Purposes</option>
                            <option>To Collect Specimen</option>
                            <option>To By Stocks For The Faculty</option>
                            <option>Other</option>
                            
                           </select>

                        </td>
                        
                    </tr>

                     <tr>
                        <td>Organized By<br></br></td>
                        <td>
                        <select  name="orgby" class="form-control">
                             <option  selected disabled>Select Organizer</option>
                            <option>NSF</option>
                            <option>UGC</option>
                            <option>SLAAS</option>
                            <option>Other</option>
                        
                    <tr>
                        <td>Requested Date<br></br></td>
                        <td><input type="date" name="req_date"  class="form-control" required=""></td> 
                    </tr>
                    <tr>
                        <td>Requested Time<br></br></td>
                        <td><input type="time" name="req_time"  class="form-control" required=""></td>
                    </tr>
                    <tr>
                        <td>From<br></br></td>
                        <td><input type="text" name="from"  class="form-control" placeholder="Start Location" required=""></td>
                        
                    </tr>
                    <tr>
                        <td>To<br></br></td>
                        <td><input type="text" name="to"  class="form-control" placeholder="End Location" required=""></td>
                    </tr>
                    <tr>
                        <td>Expected Distance (km)  <br></br></td>
                        <td><input type="text" name="distance"  class="form-control" placeholder=" Distance of Journey" required=""></td>
                        
                    </tr>


                   <tr>
                        <td>Expected Duration of Journey <br></br></td>
                        <td>
                        <select name="days">
                             <option  selected disabled>Days</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select> Days
                        <select name="hours">
                             <option selected disabled>Hours</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                             <option>4</option>
                            <option>5</option>
                            <option>6</option>
                             <option>7</option>
                            <option>8</option>
                            <option>9</option>
                        </select> Hours
                           
                        </td>
                        <td>
                        
                           
                        </td>
                       
                   </tr>

                   <tr>
                        <td>No. of Participants <br></br></td>
                        <td>
                          <input type="text" class="form-control" name="no_of_participants" placeholder="Enter NO. of Participants">
                        </td>
                        
                        
                    </tr>


                    <!-- <tr>
                        <td><br></br></td>
                        <td>
                                
                        <input id="typeahead" type="text" name="typeahead" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Search">
                          <input onclick="addParticipants()" type='submit' value='ADD'/> 
                    </tr>-->

                    <tr>
                        <td>Participants<br/><br/></td>
                        <td>                               
                            <textarea id="Participants" name="staff_participants" rows="4" cols="50"  class="form-control"></textarea>
                        </td>
                    </tr>
                    <?php
                      if(isset($_post['upload_students']))
                    ?>
                    <form method="post">
                    <tr>
                        
                        <td>Names of Students<br></br></td>
                        
                        <input type="hidden" name="size" value="1000000">

                        <td>
                          <div><input type="file" name="student_participants" placeholder="Attach Your Files"></div>
                        </td>
                        
                        
                    </tr>
                       <tr>
                        <td>Attach Invitation/Letter<br></br></td>
                        <td>
                          <div>
                            <input type="file"  name="invitation" placeholder="Attach Your Files">
                            <input type="submit" name="upload_invitaion" value="upload file">
                          </div>
                        </td>
                        
                        
                    </tr>
                  </form>

                    <tr align="right">
                    
                        <td colspan="2" >
                          <!--<form action="forword_request_action.php" method="POST">
                            <input type="hidden" name="Req_ID" value="<?php echo $Req_ID ?>"> -->
                            <input type="submit" value="Submit" name="submit" class="btn btn-inverse" style="background-color:darkblue;color:white">   
                            <button type="reset" value="Reset" class="btn btn-inverse" style="background-color:darkblue;color:white">Cancel</button>
                        <!--</form> -->
                        </td>   
                    </tr>
                <!--/.table-->
                </table>
            </form>
            <br>
        </div>
    </div>
            </div>
            <!--/.form -->           
			
		 <!--/.form --> 
                                      
     <!-- /.row -->
    </div>
    




          <!-- Icon Cards-->
          
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © TMS  CS2018g8</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave this page!</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../UserLogin/LogOut.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <script>
      function addParticipants() {
        if(document.getElementById("typeahead").value) {
          document.getElementById("Participants").value += document.getElementById("typeahead").value + ",";
        }
      }
    </script>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../SearchBox/typeahead.min.js"></script>
    <script>
    $(document).ready(function(){
    $('input.typeahead').typeahead({
        name: 'typeahead',
        remote:'../SearchBox/search.php?key=%QUERY',
        limit : 10
    });
});
    </script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
