<?php 
    session_start();
    if(isset($_SESSION['ROLE'])){
        return header("location:../");
    }
?>
<html>
<head>
        <title>User-Login-Vehical-Managment-System-University Of Ruhuna-Faculty Of Science</title>
        <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <link rel="stylesheet" type="text/css" href="Style.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="../Notify/notify.js"></script>
        
</head>
<body>
    <div class="container">
        <div class="login-box">
        <div class="row">
            
             <div class="col-md-4 login-logo"> </div>
            
            <div class="col-md-6 login-style">
                <h1><b>Login-Here</b></h1>
                <hr width="100%" color="#66CCFF" size="6"/>
                <form action='action.php' method="post">
                    <div class="form-group">
                        <input type="text" name="User_ID" placeholder="UserID" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Password" class="form-control" required>
                    </div>
                    <div class="form-group">
								<input type="checkbox" name="remember_me" id="remember_me" class="">
								<label for="remember_me"><b>Remember Me!</b></label>
                                <a href="#">Forget Password?</a> 
				    </div>
                    
                    <button type="submit" class="btn btn-primary">Login</button>
                    
                </form>

            </div>
            

            <?php
            if(isset($_SESSION['ERROR_MESSAGE'])){
                //echo $_SESSION['ERROR_MESSAGE']
        
            //<script>
                //$.notify("Username or Password incorrect.", {className: 'error'});
            //</script>
                echo "<div class='alert alert-warning'>
                      <strong>Username and/or password Error</strong>
                </div>";
            
        
            session_destroy();       
            }
            ?>
            
        </div>
        </div>
    </div>
    
    

    
</body>
</html>