<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Clerk_View_New_Requests</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="http://www.sci.ruh.ac.lk/">Faculty Of Science</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">9+</span>
          </a>
          
        </li>
        
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="profile.php">Profile</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../UserLogin/userlogin.php" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

       <div id="wrapper">

      <!-- Sidebar -->
     <ul class="sidebar navbar-nav">

       
        <li class="nav-item active ">
          <a class="nav-link" href="clerk.php">
            <i class="fas fa-fw fa-arrow-circle-right"></i>
            <span>Pending Request</span>
          </a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link" href="oldrequest.php">
            <i class="fas fa-fw fa-align-right"></i>
            <span>Old Request</span></a>
        </li>
         
        <li class="nav-item">
          <a class="nav-link" href="Admin.php">
            <i class="fas fa-fw fa-save"></i>
            <span>Add New User</span></a>
        </li>
        
      </ul>
      <div id="content-wrapper">

        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a>Request Details</a>
            </li>
           
          </ol>
          

          <div class="container">           
  <table class="table">
    
    <tbody>
      <tr>
        <td>Request ID</td>
        <td>1</td>
       
        <td>Name</td>
        <td>D.W.Perera</td>
       
      </tr>
      <tr>
        <td>Employee No</td>
        <td>FC/SC/1953</td>
       
        <td>Reason in Brief</td>
        <td>For a Meeting</td>
       
      </tr>
      <tr>
        <td>Organized By</td>
        <td>UGC</td>
       
        <td>Reqested Date</td>
        <td>07/12/2018</td>
       
      </tr>
      <tr>
        <td>Requested Time</td>
        <td>08:00</td>
       
        <td>From</td>
        <td>University</td>
       
      </tr>
      <tr>
        <td>To</td>
        <td>Colombo</td>

        <td>Expected Distance(km)</td>
        <td>170</td>
      </tr>
      <tr>
        <td>Expected Duration of Journey</td>
        <td>1 Day</td>

        <td>No.Of Participants</td>
        <td>5</td>
      </tr>

 <tr>
        <td>Assign Driver</td>
        <td>saman</td>

        <td>Assign Vehicle</td>
        <td>AC3423</td>
      </tr>
        
         </tbody>
                </table>
             



            </div>
            <div class="modal-footer">
                
                 <button type="button" class="btn btn-success " data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

           
<script>$('#myModal').on('shown.bs.modal', function () {
$('#myInput').trigger('focus')
})</script>
</td></tr>



       
    </tbody>
  </table>
</div>
		

          
          <div class="buttton" >
           <a href="clerk.php">
           <input type="submit" value="Forward" class="btn btn-success" style="float:right;"> </a>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#errorreason">
    Return
</button>
           
<div class="modal fade" id="errorreason" tabindex="-1" role="dialog" aria-labelledby="errorreason" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="errorreason">Error!!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               Reason=>
               <input type="text">
            </div>
            <div class="modal-footer">
              <a href="clerk.php"><button type="button" value="More" class="btn btn-success" style="color:white">Ok</button></a>
               
            </div>
        </div>
    </div>
</div>

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span></span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../UserLogin/userlogin.php">Logout</a>
          </div>
        </div>
      </div>
    </div>






    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
