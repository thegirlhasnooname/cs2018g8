<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Clerk_New_Request</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="http://www.sci.ruh.ac.lk/">Faculty Of Science</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">9+</span>
          </a>
          
        </li>
        
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="profile.php"> Profile</a>
           
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../UserLogin/userlogin.php" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">

       
        <li class="nav-item">
          <a class="nav-link" href="clerk.php">
            <i class="fas fa-fw fa-arrow-circle-right"></i>
            <span>Pending Request</span>
          </a>
        </li>
       
        <li class="nav-item active">
          <a class="nav-link" href="oldrequest.php">
            <i class="fas fa-fw fa-align-right"></i>
            <span>Old Request</span></a>
        </li>
          
        <li class="nav-item">
          <a class="nav-link" href="Admin.php">
            <i class="fas fa-fw fa-save"></i>
            <span>Add New User</span></a>
        </li>
        
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a ><h3>Old Request</h3></a>
            </li>
           
          </ol>
			
			<div id="page-wrapper" style="background-color:lightgrey;color:black">
            
           
      <div class="card mb-3">
            <div class="card-header">
              
<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Rejected Request
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
	<li><a href="oldrequest.php">All Request</a></li>
	<li><a href="approvedrequest.php">Approved Request</a></li>
	<li><a href="rejectedrequest.php">Rejected Request</a></li>
    </ul>
  </div>


        </div>
         
            <div class="card-body">

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name of Requester</th>
                      <th>Reason</th>
                      <th>Required Date</th>
                      <th>Distance</th>
					            <th>Destination</th>
					            <th>More</th>
					  
                    </tr>
                  </thead>
                 
                  <tbody>
                    <tr>
                      <td>Nimal</td>
                      <td>For a Meeting</td>
                      <td>2018/12/12</td>
					            <td>250KM</td>
                      <td>Colombo</td>
                      <td> <a href="viewallrequest.php"><button type="button" value="More" class="btn btn-info" style="background-color:;color:white">View</button></a></td>
                    </tr>
                   <tr>
                      <td>Amal</td>
                      <td>For Reaseach Purposes</td>
                      <td>2018/12/15</td>
                      <td>150KM</td>
                      <td>Galle</td>
                       <td> <a href="viewallrequest.php"><button type="button" value="More" class="btn btn-info" style="background-color:;color:white">View</button></a></td>
                    </tr>
                    </tr>
                    <tr>
                      <td>Kamal</td>
                      <td>To Collect Specimen</td>
                      <td>2018/12/18</td>
                      <td>50KM</td>
                      <td>Matara</td>
                       <td> <a href="viewallrequest.php"><button type="button" value="More" class="btn btn-info" style="background-color:;color:white">View</button></a></td>
                    </tr>
                    </tr>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
  

        </div>
                <div/>
            </div>


      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
        

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © TMS  CS2018g8</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../UserLogin/userlogin.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
